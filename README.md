# rofi-nm-kmenu
The script for rofi, that allows turning Network Manager connections on and off.

The menu contains:
* Currently active connections
* History of connections toggled by rofi-nm-kmenu
* Submenus for all the types of available connections

## Build and install
Run gradle build:
```
./gradlew build
```

Then copy `./build/bin/native/releaseExecutable` to your `PATH`

## Usage
```
rofi -show nm -modi "nm:rofi-nm-kmenu.kexe"
```

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/F2F16RQKM)