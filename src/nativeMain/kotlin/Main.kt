import kotlinx.coroutines.runBlocking
import space.dlowl.kmenu.menu.Menu
import space.dlowl.rofinmkmenu.cache.Cache
import space.dlowl.rofinmkmenu.cache.CacheItem
import space.dlowl.rofinmkmenu.nm.NetworkManager

fun toggle(uuid: String, active: Boolean, cache: Cache) {
    if (active) {
        NetworkManager.downConnection(uuid)
    } else {
        NetworkManager.upConnection(uuid)
    }

    runBlocking {
        cache.add(CacheItem(uuid))
        cache.save()
    }
}

fun main(args: Array<String>) {
    val cache: Cache = runBlocking { Cache.load() }
    val connections = NetworkManager.getConnections()
    val types = connections.map { it.type }.toSet()
    Menu.menu {
        option {
            label = "--- Active Connections ---"
            nonselectable = true
        }
        connections.filter { it.active }.forEach {
            option {
                label = it.printableName
                key = it.uuid
                func = { _ -> toggle(it.uuid, it.active, cache) }
            }
        }
        option {
            label = "--- History ---"
            nonselectable = true
        }
        cache.items.map {
            Pair(it.uuid, connections.find { network -> network.uuid == it.uuid })
        }.forEach { (uuid, network) ->
            if (network == null) {
                option {
                    label = "Unavailable network: ${uuid}"
                    nonselectable = true
                }
            } else {
                option {
                    label = network.printableName
                    key = network.uuid
                    func = { _ -> toggle(network.uuid, network.active, cache) }
                }
            }
        }
        option {
            label = "--- Menu ---"
            nonselectable = true
        }
        types.forEach { type ->
            submenu {
                title = "$type connections"
                key = "${type}_connections"
                connections.filter { connection -> connection.type == type }.forEach {
                    option {
                        label = it.printableName
                        key = it.uuid
                        func = { _ -> toggle(it.uuid, it.active, cache) }
                    }
                }
            }
        }

    }.main(args)
}