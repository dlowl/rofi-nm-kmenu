package space.dlowl.rofinmkmenu.nm

data class Network(val name: String, val uuid: String, val type: String, val device: String, var active: Boolean = false){
    val printableName: String
        get() = if (active) "$name (active)" else name
}
