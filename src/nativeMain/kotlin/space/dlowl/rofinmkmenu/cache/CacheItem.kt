package space.dlowl.rofinmkmenu.cache

import kotlinx.serialization.Serializable

@Serializable
data class CacheItem(val uuid: String)