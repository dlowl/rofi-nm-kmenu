package space.dlowl.rofinmkmenu.cache

import com.soywiz.korio.async.async
import com.soywiz.korio.file.VfsFile
import com.soywiz.korio.file.std.localVfs
import com.soywiz.korio.file.std.userHomeVfs
import com.soywiz.korio.lang.FileNotFoundException
import kotlinx.cinterop.toKString
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import platform.posix.fopen
import platform.posix.getenv

class Cache(var items: List<CacheItem>, val size: Int = 3) {

    fun add(item: CacheItem) {
        var remainingItems = items.filter { it != item }
        if (remainingItems.size >= size) {
            remainingItems = remainingItems.dropLast(remainingItems.size - size + 1)
        }
        items = listOf(item) + remainingItems
    }

    fun serialise(): String = Json.encodeToString(items)

    suspend fun save() {
        getPath().writeString(serialise())
    }

    companion object {
        suspend fun getPath(): VfsFile {
            var path = localVfs(getenv("HOME")!!.toKString())[".cache"]
            if (!path.exists()) {
                path.mkdir()
            }
            path = path["rofikmenu"]
            if (!path.exists()) {
                path.mkdir()
            }
            return path["cache.json"]
        }

        suspend fun load(): Cache =
            try {
                Cache(deserialise(getPath().readString()))
            } catch (e: FileNotFoundException) {
                Cache(listOf())
            }

        private fun deserialise(s: String) = Json.decodeFromString<List<CacheItem>>(s)
    }
}