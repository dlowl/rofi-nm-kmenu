plugins {
    kotlin("multiplatform") version "1.5.31"
    kotlin("plugin.serialization") version "1.5.31"
}

group = "space.dlowl"
version = "1.0"

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/projects/30308398/packages/maven")
}

kotlin {
    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        binaries {
            executable {
                entryPoint = "main"
            }
        }
    }
    sourceSets {
        val nativeMain by getting {
            dependencies {
                implementation("space.dlowl:kmenu-native:1.1")
                implementation("com.soywiz.korlibs.korio:korio:2.2.0")
            }
        }
        val nativeTest by getting
    }
}
